Overview
--------
The IRCHWebchat module allows you to integrate IRC-Hispano webchat on your
Drupal site.


Requirements
------------
Drupal 6.x


Features
--------
Provides a block with a login form to the chat and a page with an embeded chat.
Ability to change configuration.


Installation
------------
Copy to your modules folder and activate it on Drupal.


Credits
-------
 - Created and mantained by Ruharen
 

Bugs and Suggestions
--------------------
Bug reports, support requests, feature requests, etc, should be posted to
  IRCHWebchat module project page:
  http://drupal.org/project/irchwebchat
